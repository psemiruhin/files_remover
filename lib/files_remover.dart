library files_deleting.transformer;

import 'dart:async';
import 'package:barback/barback.dart';
import 'dart:io';

/// Transformer used by `pub build` to remove files.
class FilesRemoverTransformer extends Transformer {
  final BarbackSettings settings;
  final TransformerOptions options;

  FilesRemoverTransformer(BarbackSettings settings) :
    settings = settings,
    options = new TransformerOptions.parse(settings.configuration);

  FilesRemoverTransformer.asPlugin(BarbackSettings settings) :
    this(settings);

  Future<bool> isPrimary(AssetId input) {
    bool primary = false;
    if (options.files != null) {
      for (String fileName in options.files) {
        if (fileName.contains(input.path)) {
          primary = true;
          break;
        }
      }
    }
    return new Future.value(primary);
  }

  Future apply(Transform transform) {
    String path;

    for (String fileName in options.files) {
      if (fileName.contains(transform.primaryInput.id.path)) {
        path = fileName;
        options.files.remove(fileName);
        break;
      }
    }
    
    transform.consumePrimary();

    File file = new File(path);
    return file.delete().then((FileSystemEntity result) {
      print('removed ' + path);
      return;
    }).catchError((Exception e) {
      print(e.toString());
    });
  }
}

class TransformerOptions {
  final List<String> files;

  TransformerOptions({List<String> this.files});

  factory TransformerOptions.parse(Map configuration) {
    config(key, defaultValue) {
      var value = configuration[key];
      return value != null ? value : defaultValue;
    }

    return new TransformerOptions(
        files: config("files", []));
  }
}
