Transformer that will remove list of files.

## Usage

Simply add the following lines to your `pubspec.yaml`:

    :::yaml
    dependencies:
      files_remover: any
    transformers:
      - files_remover

## Configuration

You can specify list of files:

    :::yaml
    transformers:
      - files_remover:
          files: 
          - /path/to/file_1
          - /path/to/file_2
          - /path/to/file_n
